﻿using common.Messenger;

namespace server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var messenger = new Messenger();
            var server = new Server(messenger);
            server.Run();
        }
    }
}