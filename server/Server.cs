﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using common;
using common.Messenger;

namespace server
{
    public class Server
    {
        private readonly IMessenger _messenger;

        private readonly ConcurrentDictionary<int, ServerClient> _clients;

        public IEnumerable<int> Clients => _clients.Keys;

        public Server(IMessenger messenger)
        {
            _messenger = messenger;
            _clients = new ConcurrentDictionary<int, ServerClient>();
        }

        public void Run()
        {
            _messenger.RunServer(OnClientConnected);
            Console.WriteLine("Server running");

            foreach (var message in _messenger.GetMessagesFromClients())
            {
                // Handle disconnection
                if (message.Type == MessageType.Disconnect)
                {
                    _clients.TryRemove(message.ClientId, out _);
                    Console.Write($"Client {message.ClientId} disconnected.");
                    continue;
                }

                if (message.Data is null || !_clients.TryGetValue(message.ClientId, out var client))
                    continue;

                foreach (var messageString in client.BuildMessage(message.Data))
                    HandleMessage(client, messageString);
            }
        }

        private void OnClientConnected(int id)
        {
            var client = new ServerClient(id, _messenger);
            _clients[id] = client;
            MessageClient(client, Protocol.Welcome);
        }

        private void HandleMessage(ServerClient client, string message)
        {
            if (client.Nickname is null)
            {
                var nickname = message;

                // Check if there's another client with the same nick
                if (_clients.Values.Any(c => c.Nickname == nickname))
                    MessageClient(client, Protocol.NicknameAlreadyInUse(nickname));
                else
                {
                    MessageClient(client, Protocol.RegisteredAs(nickname));
                    Broadcast(Protocol.HasJoined(nickname));
                    client.Nickname = nickname;
                    Console.WriteLine($"Client id {client.Id} has joined as {client.Nickname}");
                }

                return;
            }

            Broadcast(Protocol.UserSays(client.Nickname!, message));
        }

        private void Broadcast(string message)
        {
            foreach (var client in _clients.Values)
                MessageClient(client, message);
        }

        private void MessageClient(ServerClient client, string message)
        {
            var success = client.SendMessage(message);

            // Client has disconnected
            if (!success)
                _clients.TryRemove(client.Id, out _);
        }
    }
}