﻿using System.Collections.Generic;
using common;
using common.Messenger;

namespace server
{
    public class ServerClient
    {
        private readonly IMessenger _messenger;

        private readonly MessageBuilder _builder;

        public string? Nickname { get; set; }

        public int Id { get; }

        public ServerClient(int id, IMessenger messenger)
        {
            Id = id;
            _messenger = messenger;
            _builder = new MessageBuilder();
        }

        public bool SendMessage(string message) =>
            _messenger.MessageClient(Id, Utils.ByteMessage(message));

        public IEnumerable<string> BuildMessage(byte[] data)
        {
            _builder.Add(data);
            return _builder.Build();
        }
    }
}