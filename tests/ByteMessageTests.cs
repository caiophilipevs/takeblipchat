using System;
using System.Collections.Generic;
using FluentAssertions;
using Xunit;
using static common.Utils;
using static common.Protocol;

namespace tests
{
    public class ByteMessageTests
    {
        public static TheoryData<string, byte[]> EncodeDataData() => new()
        {
            {"", Array.Empty<byte>()},
            {"a", new byte[] {0x61}},
            {"ab", new byte[] {0x61, 0x62}},
            {"caio", new byte[] {0x63, 0x61, 0x69, 0x6f}},
            {"phil", new byte[] {0x70, 0x68, 0x69, 0x6c}},
        };

        [Theory]
        [MemberData(nameof(EncodeDataData))]
        public void EncodeData(string message, IEnumerable<byte> expected)
        {
            var actual = ByteData(message);
            actual.Should().BeEquivalentTo(expected, options => options.WithStrictOrdering());
        }

        public static TheoryData<string, byte[]> EncodeMessageData() => new()
        {
            {"", new[] {MessageSeparator}},
            {"a", new byte[] {0x61, MessageSeparator}},
            {"ab", new byte[] {0x61, 0x62, MessageSeparator}},
            {"caio", new byte[] {0x63, 0x61, 0x69, 0x6f, MessageSeparator}},
            {"phil", new byte[] {0x70, 0x68, 0x69, 0x6c, MessageSeparator}},
        };

        [Theory]
        [MemberData(nameof(EncodeMessageData))]
        public void EncodeMessage(string message, IEnumerable<byte> expected)
        {
            var actual = ByteMessage(message);
            actual.Should().BeEquivalentTo(expected, options => options.WithStrictOrdering());
        }
    }
}