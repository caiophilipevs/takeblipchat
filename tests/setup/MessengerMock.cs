﻿using System;
using System.Collections.Generic;
using System.Linq;
using common.Messenger;

namespace tests.setup
{
    public class MessengerMock : IMessenger
    {
        // List of clients that has connected and not being processed by the server 
        private readonly List<int> _clientsConnected;

        // Id of the next client to connect
        private int _clientNextId;

        // Messages sent from the clients to the server
        private readonly Dictionary<int, List<byte[]>> _messagesFromClients;

        // Messages sent from the server to the clients
        private readonly Dictionary<int, List<byte[]>> _messagesToClients;

        public MessengerMock()
        {
            _clientsConnected = new List<int>();
            _clientNextId = 1;

            _messagesFromClients = new Dictionary<int, List<byte[]>>();
            _messagesToClients = new Dictionary<int, List<byte[]>>();
        }

        #region Server

        public void RunServer(Action<int> onClientConnected)
        {
            foreach (var client in _clientsConnected)
                onClientConnected(client);
            _clientsConnected.Clear();
        }

        // FIXME: Mock disconnect messages
        public IEnumerable<Message> GetMessagesFromClients()
        {
            var res = _messagesFromClients
                .SelectMany(entry =>
                    entry.Value
                        .Select(message => new Message(entry.Key, MessageType.Data, message))
                )
                .ToList();

            // Remove all messages since they'll all be processed by the server
            foreach (var messages in _messagesFromClients.Values)
                messages.Clear();

            return res;
        }

        public bool MessageClient(int id, byte[] data)
        {
            if (!_messagesToClients.TryGetValue(id, out var client))
                return false;

            client.Add(data);
            return true;
        }

        #endregion

        #region Client

        public int ConnectToServer()
        {
            var clientId = _clientNextId;
            _clientNextId++;

            _clientsConnected.Add(clientId);
            _messagesFromClients.Add(clientId, new List<byte[]>());
            _messagesToClients.Add(clientId, new List<byte[]>());

            return clientId;
        }

        public void MessageServer(int clientId, byte[] data)
        {
            if (_messagesFromClients.TryGetValue(clientId, out var server))
                server.Add(data);
        }

        public IEnumerable<byte[]> GetMessagesFromServer(int clientId)
        {
            if (!_messagesToClients.TryGetValue(clientId, out var messages))
                return Enumerable.Empty<byte[]>();

            _messagesToClients[clientId] = new List<byte[]>();
            return messages;
        }

        #endregion
    }
}