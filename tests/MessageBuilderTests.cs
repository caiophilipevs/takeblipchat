﻿using System;
using System.Collections.Generic;
using System.Linq;
using common;
using FluentAssertions;
using Xunit;

namespace tests
{
    public class MessageBuilderTests
    {
        private static byte[] _(params char[] c) =>
            c.Select(x => (byte) x).ToArray();

        public static TheoryData<byte[][], string[]> AddMessagesData() => new()
        {
            // Empty data
            {Array.Empty<byte[]>(), Array.Empty<string>()},
            {new[] {Array.Empty<byte>()}, Array.Empty<string>()},

            // Single call
            {new[] {_('c', 'a', 'i', 'o')}, Array.Empty<string>()},
            {new[] {_('\xff')}, new[] {""}},
            {new[] {_('c', 'a', 'i', 'o', '\xff')}, new[] {"caio"}},
            {new[] {_('\xff', '\xff')}, new[] {"", ""}},
            {new[] {_('c', 'a', 'i', 'o', '\xff', 's', 'o', 'u', 'z', 'a')}, new[] {"caio"}},
            {new[] {_('c', 'a', 'i', 'o', '\xff', 's', 'o', 'u', 'z', 'a', '\xff')}, new[] {"caio", "souza"}},
            {
                new[] {_('c', 'a', 'i', 'o', '\xff', '\xff', 's', 'o', 'u', 'z', 'a', '\xff')},
                new[] {"caio", "", "souza"}
            },
            {new[] {_('\xff', 'c', 'a', 'i', 'o', '\xff', 's', 'o', 'u', 'z', 'a')}, new[] {"", "caio"}},

            // Multiple calls
            {new[] {_(), _()}, Array.Empty<string>()},
            {new[] {_(), _('\xff')}, new string[] {""}},
            {new[] {_('\xff'), _('\xff')}, new string[] {"", ""}},
            {new[] {_('c', 'a', 'i', 'o'), _('\xff')}, new[] {"caio"}},
            {new[] {_('c', 'a', 'i', 'o'), _('\xff'), _('s', 'o', 'u', 'z', 'a')}, new[] {"caio"}},
            {new[] {_('c', 'a', 'i', 'o'), _('\xff', 's', 'o', 'u', 'z', 'a'), _('\xff')}, new[] {"caio", "souza"}},
            {new[] {_('c', 'a', 'i', 'o'), _('\xff'), _('s', 'o', 'u', 'z', 'a'), _('\xff')}, new[] {"caio", "souza"}},
        };

        [Theory]
        [MemberData(nameof(AddMessagesData))]
        public void AddMessages(IEnumerable<byte[]> messages, IEnumerable<string> expected)
        {
            // Arrange
            var server = new MessageBuilder();
            foreach (var message in messages)
                server.Add(message);

            // Act
            var actual = server.Build();

            // Assert
            actual.Should().BeEquivalentTo(expected, options => options.WithStrictOrdering());
        }
    }
}