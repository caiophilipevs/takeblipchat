﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using client;
using common;
using common.Messenger;
using FluentAssertions;
using server;
using tests.setup;
using Xunit;

namespace tests
{
    [SuppressMessage("ReSharper", "CoVariantArrayConversion", Justification = "These are used on FluentAssertions")]
    public class ServerLoginTest
    {
        private readonly IMessenger _messenger;

        private readonly Server _server;

        public ServerLoginTest()
        {
            _messenger = new MessengerMock();
            _server = new Server(_messenger);
        }

        public static TheoryData<int, IEnumerable<int>> ClientListData() => new()
        {
            {0, Array.Empty<int>()},
            {1, new[] {1}},
            {2, new[] {1, 2}},
        };

        // All users have successfully logged in 
        [Theory]
        [MemberData(nameof(ClientListData))]
        public void ClientList(int clientAmount, IEnumerable<int> expected)
        {
            // Arrange
            for (var i = 1; i <= clientAmount; i++)
                _ = new Client(_messenger);

            // Act
            _server.Run();

            // Assert
            _server.Clients.Should().BeEquivalentTo(expected);
        }

        // Check login messages
        [Fact]
        public void LoginMessages()
        {
            // Arrange
            const string nick = "caio.souza";
            var client = new Client(_messenger);
            client.Send(nick);

            var otherClient = new Client(_messenger);

            // Act
            _server.Run();
            client.Run();
            otherClient.Run();

            // Assert
            client.Messages.Should().BeEquivalentTo(new[]
            {
                Protocol.Welcome,
                Protocol.RegisteredAs(nick),
                Protocol.HasJoined(nick)
            }, options => options.WithStrictOrdering());

            otherClient.Messages.Should().BeEquivalentTo(new[]
            {
                Protocol.Welcome,
                Protocol.HasJoined(nick)
            }, options => options.WithStrictOrdering());
        }

        // Check nickname already in use
        [Fact]
        public void NickAlreadyInUse()
        {
            // Arrange
            var nick = "caio.souza";
            var client = new Client(_messenger);
            client.Send(nick);

            var otherNick = "phil.souza";
            var otherClient = new Client(_messenger);
            otherClient.Send(nick);
            otherClient.Send(otherNick);

            // Act
            _server.Run();
            client.Run();
            otherClient.Run();

            // Assert
            client.Messages.Should().BeEquivalentTo(new[]
            {
                Protocol.Welcome,
                Protocol.RegisteredAs(nick),
                Protocol.HasJoined(nick),
                Protocol.HasJoined(otherNick)
            }, options => options.WithStrictOrdering());

            otherClient.Messages.Should().BeEquivalentTo(new[]
            {
                Protocol.Welcome,
                Protocol.HasJoined(nick),
                Protocol.NicknameAlreadyInUse(nick),
                Protocol.RegisteredAs(otherNick),
                Protocol.HasJoined(otherNick),
            }, options => options.WithStrictOrdering());
        }
    }
}