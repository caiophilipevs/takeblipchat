﻿using System;
using System.Collections.Generic;
using System.Text;

namespace common
{
    public class MessageBuilder
    {
        // List of messages ready to be consumed
        private List<string> _messages;

        // Message being processed
        private byte[] _lastMessage;

        public MessageBuilder()
        {
            _messages = new List<string>();
            _lastMessage = Array.Empty<byte>();
        }

        // Add a message be consume later
        // FIXME: Check max size of messages to avoid an attack in which an arbitrarily large message is sent and
        // the server run out of memory
        public void Add(byte[] message)
        {
            var startMessage = 0;
            for (var i = 0; i < message.Length; i++)
            {
                // Look for the separator
                if (message[i] != Protocol.MessageSeparator)
                    continue;

                // Concat the current message with any previous one that might exist
                var splitMessage = new byte[_lastMessage.Length + i - startMessage];

                Buffer.BlockCopy(_lastMessage, 0, splitMessage, 0, _lastMessage.Length);
                Buffer.BlockCopy(message, startMessage, splitMessage, _lastMessage.Length, i - startMessage);

                var messageString = Encoding.UTF8.GetString(splitMessage);
                _messages.Add(messageString);

                // Last message has been used, so now it's empty
                _lastMessage = Array.Empty<byte>();

                // Next message started at `i`, plus one to offset the separator
                startMessage = i + 1;
            }

            // Add the rest of the message to be processed next
            _lastMessage = new byte[message.Length - startMessage];
            Buffer.BlockCopy(message, startMessage, _lastMessage, 0, _lastMessage.Length);
        }

        // Return the list of messages available for consumption and clear them
        public IEnumerable<string> Build()
        {
            var res = _messages;
            _messages = new List<string>();
            return res;
        }
    }
}