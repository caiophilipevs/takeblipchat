﻿using System.Text;

namespace common
{
    public static class Utils
    {
        // Convert a string into its bytearray representation
        public static byte[] ByteData(string message) =>
            Encoding.Default.GetBytes(message);

        // Create a byte message from string by appending the separator at the end
        // FIXME: Find a better way to this so it won't allocate a bytearray twice.
        public static byte[] ByteMessage(string message)
        {
            var bytes = ByteData(message);
            var res = new byte[bytes.Length + 1];
            bytes.CopyTo(res, 0);
            res[bytes.Length] = Protocol.MessageSeparator;
            return res;
        }
    }
}