﻿namespace common
{
    public static class Protocol
    {
        // 0xff can't appear in UTF-8 strings, so it's used as a separator for the messages
        public const byte MessageSeparator = 0xff;

        public const string Welcome = "*** Welcome to our chat server. Please provide a nickname:";

        public static string NicknameAlreadyInUse(string nick) =>
            $"*** Sorry, then nickname {nick} is already taken. Please choose a different one:";

        public static string RegisteredAs(string nick) =>
            $"*** You are registered as {nick}. Joining #general";

        public static string HasJoined(string nick) =>
            $"\"{nick}\" has joined #general";

        public static string UserSays(string nick, string message) =>
            $"\"{nick}\" says: {message}";
    }
}