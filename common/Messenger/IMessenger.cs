﻿using System;
using System.Collections.Generic;

namespace common.Messenger
{
    // FIXME: This interface has too many responsibilities.
    // It should be divide in two to better accomodate the client/server distinction.
    // It was done this way to simplify the communication mockup for testing.
    public interface IMessenger
    {
        #region Server

        // Runs the server
        public void RunServer(Action<int> onClientConnected);

        // Get the messages received by the server
        public IEnumerable<Message> GetMessagesFromClients();

        // Send a message to the client identified. Returns if it was successful.
        bool MessageClient(int id, byte[] data);

        #endregion

        #region Client

        // Connect to the server
        int ConnectToServer();

        // Send a message to the server
        void MessageServer(int clientId, byte[] data);

        // Read messages sent from the server
        public IEnumerable<byte[]> GetMessagesFromServer(int clientId);

        #endregion
    }
}