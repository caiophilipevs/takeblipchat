﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace common.Messenger
{
    public class Messenger : IMessenger
    {
        #region Server

        // Used when it's a server to listen for client connections
        private TcpListener _tcpListener = null!;

        // Used when it's a server gather the client connections
        private ConcurrentDictionary<int, TcpClient> _clients = null!;

        public void RunServer(Action<int> onClientConnected)
        {
            _clients = new ConcurrentDictionary<int, TcpClient>();
            _tcpListener = new TcpListener(IPAddress.Parse("127.0.0.1"), 50107);
            _tcpListener.Start();

            // Accept clients in a new thread because this is a blocking operation
            new Thread(() => AcceptClients(onClientConnected)).Start();
        }

        // FIXME: Check for program end and return from this method
        [SuppressMessage("ReSharper", "FunctionNeverReturns", Justification = "Will be fixed later")]
        private void AcceptClients(Action<int> onClientConnected)
        {
            var id = 1;

            while (true)
            {
                // Wait for a client to connect
                var tcpClient = _tcpListener.AcceptTcpClient();
                _clients.TryAdd(id, tcpClient);
                onClientConnected(id);
                Console.WriteLine($"Client id {id} connected");
                id++;
            }
        }

        // FIXME: Check for program end and return from this method
        [SuppressMessage("ReSharper", "IteratorNeverReturns", Justification = "Will be fixed later")]
        public IEnumerable<Message> GetMessagesFromClients()
        {
            while (true)
            {
                foreach (var (id, client) in _clients)
                {
                    NetworkStream? stream = null;
                    try
                    {
                        stream = client.GetStream();
                    }
                    catch (InvalidOperationException)
                    {
                        // Handled below because "yield return" can't be invoke from a catch block.
                    }

                    // Client has disconnected
                    if (stream is null)
                    {
                        _clients.TryRemove(id, out _);
                        yield return new Message(id, MessageType.Disconnect, null);
                        continue;
                    }

                    if (!stream.DataAvailable)
                        continue;

                    var data = new byte[client.Available];
                    stream.Read(data, 0, data.Length);
                    yield return new Message(id, MessageType.Data, data);
                }

                Thread.Sleep(10);
            }
        }

        public bool MessageClient(int id, byte[] data)
        {
            var client = _clients[id];

            try
            {
                var stream = client.GetStream();
                stream.Write(data, 0, data.Length);
                return true;
            }
            catch (Exception ex)when (ex is InvalidOperationException or IOException)
            {
                // Client has disconnected
                return false;
            }
        }

        #endregion

        #region Client

        // Used when it's a client connecting to the server
        private readonly List<TcpClient> _tcpServers = new List<TcpClient>();

        public int ConnectToServer()
        {
            var id = _tcpServers.Count;
            _tcpServers.Add(new TcpClient("127.0.0.1", 50107));
            return id;
        }

        public void MessageServer(int clientId, byte[] data)
        {
            var tcpServer = _tcpServers[clientId];
            var stream = tcpServer.GetStream();
            stream.Write(data, 0, data.Length);
        }

        // FIXME: Handle server disconnection
        // FIXME: Check for program end and finish this iterator
        [SuppressMessage("ReSharper", "IteratorNeverReturns", Justification = "Will be fixed later")]
        public IEnumerable<byte[]> GetMessagesFromServer(int clientId)
        {
            var server = _tcpServers[clientId];
            var stream = server.GetStream();

            while (true)
            {
                while (!stream.DataAvailable)
                {
                    Thread.Sleep(10);
                }

                var data = new byte[server.Available];
                stream.Read(data, 0, data.Length);
                yield return data;
            }
        }

        #endregion
    }
}