﻿namespace common.Messenger
{
    public enum MessageType
    {
        Disconnect,
        Data
    }

    public class Message
    {
        public int ClientId { get; set; }

        public MessageType Type { get; set; }

        public byte[]? Data { get; set; }

        public Message(int clientId, MessageType type, byte[]? data)
        {
            ClientId = clientId;
            Type = type;
            Data = data;
        }
    }
}