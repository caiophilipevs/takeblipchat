﻿using System;
using System.Linq;
using System.Threading;
using common.Messenger;

namespace client
{
    class Program
    {
        public static void Main(string[] args)
        {
            var messenger = new Messenger();
            var client = new Client(messenger);
            new Thread(() => Prompt(client)).Start();
            client.Run();
        }

        private static void Prompt(Client client)
        {
            // Wait a message to arrive
            var lastMessage = client.Messages.Count();

            void WaitResponse()
            {
                while (client.Messages.Count() == lastMessage)
                    Thread.Sleep(10);

                lastMessage = client.Messages.Count();
            }

            while (true)
            {
                WaitResponse();
                Console.Write("> ");
                var message = Console.ReadLine();
                if (message is null)
                    return;
                client.Send(message);
            }
        }
    }
}