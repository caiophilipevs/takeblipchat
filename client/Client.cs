﻿using System;
using System.Collections.Generic;
using common;
using common.Messenger;

namespace client
{
    public class Client
    {
        private readonly List<string> _messages;
        public IEnumerable<string> Messages => _messages;

        private readonly MessageBuilder _builder;

        private readonly IMessenger _messenger;

        private readonly int _id;

        public Client(IMessenger messenger)
        {
            _messages = new List<string>();
            _builder = new MessageBuilder();

            _messenger = messenger;
            _id = _messenger.ConnectToServer();
        }

        public void Send(string message)
        {
            var data = Utils.ByteMessage(message);
            _messenger.MessageServer(_id, data);
        }

        public void Run()
        {
            foreach (var data in _messenger.GetMessagesFromServer(_id))
            {
                _builder.Add(data);
                foreach (var message in _builder.Build())
                {
                    _messages.Add(message);
                    Console.WriteLine(message);
                }
            }
        }
    }
}