# Chat Take Blip

## Executar o servidor

 dotnet run --project server

## Executar o cliente

 dotnet run --project client

## Programa

Foi implementado um chat em cliente/servidor usando a biblioteca TCP do .NET.

Devidos a diversos compromissos que tive essa semana não consegui implementar a solução com a qualidade que desejava.

Também não trabalhava há tempo diretamente com TCP, então tive algumas dificuldades iniciais.

Com o intuito de apresentar o meu conhecimento em programação e apresentar as melhores práticas de programação, tentei abstrair e componentizar o máximo possível a implementação. Isso foi útil na apresentação do meu conhecimento, entretanto gerou dificuldades na implementação. Em uma situação real, faria uma teste ou protótipo inicial sem se preocupar com todas estas melhores práticas e faria sua refatoração (ou até mesmo total reimplementação) seguindo estas melhores práticas.

Espero que ainda assim tenha sido possível representar minha habilidades e estou disposto a apresentar e discutir a minha implementação pessoalmente.